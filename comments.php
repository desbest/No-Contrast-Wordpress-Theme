<?php  if ( !empty($post->post_password) && $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>
<p><?php  _e('Enter your password to view comments.'); ?></p>
<?php  return; endif; ?>
<?php $oddcomment = 'alt'; ?>

<?php if ( $comments) : ?>

<h2 id="comments"><?php comments_number(__('No comments'), __('One comment'), __('% Comments')); ?>
<?php if ( comments_open() ) : ?>
	<a href="#postcomment" title="<?php _e("Leave a comment"); ?>"></a>
<?php endif; ?>
</h2>

<ol id="commentlist">
<?php foreach ($comments as $comment) : ?>
<?php $comment_type = get_comment_type(); ?>
<?php if($comment_type == 'comment') { ?>

	<li id="comment-<?php comment_ID() ?>"  class="<?php echo $oddcomment; ?> blog_com">
	<div class="autor_comm"><b><?php comment_author_link() ?></b> &#8212; <?php comment_date('j M Y') ?> @ <a href="#comment-<?php comment_ID() ?>"><?php comment_time() ?></a><?php if(function_exists("yus_reply")) yus_reply(); ?></div>
	
<?php comment_text() ?>
	</li>
<?php /* Changes every other comment to a different class */
	if ('alt' == $oddcomment) $oddcomment = '';
	else $oddcomment = 'alt';
?>
<?php } else { $trackback = true; } ?>
<?php endforeach; ?>
</ol>

<?php if ($trackback == true) { ?>
<?php foreach ($comments as $comment) : ?>
<?php $comment_type = get_comment_type(); ?>
<?php if($comment_type != 'comment') { ?>
<div class="trackbacks"><?php comment_author_link() ?></div>
<?php } ?>
<?php endforeach; ?>
<?php } ?>

<?php else : // If there are no comments yet ?>

<?php endif; ?>

<?php if ( comments_open() ) : ?>
<a name="respond"></a><h2 id="postcomment"><?php _e('Leave a comment'); ?></h2>
<div id="form">
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.'), get_option('siteurl')."/wp-login.php?redirect_to=".urlencode(get_permalink()));?></p>
<?php else : ?>
<div id="formCONTACT">

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( $user_ID ) : ?>

<p><?php printf(__('Logged as %s.'), '<a href="'.get_option('siteurl').'/wp-admin/profile.php">'.$user_identity.'</a>'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="<?php _e('Log out of this account') ?>"><?php _e('Log out &raquo;'); ?></a></p>

<?php else : ?>
<p><b>Name</b><br/><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" /></p>
<p><b>E-mail</b><br/><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" /></p>
<p><b>URL</b><br/><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" /></p>
<?php endif; ?>
<p><textarea name="comment" cols="100%" rows="5" tabindex="4" style="overflow:hidden;" id="comment"></textarea></p>
<p><input name="submit" type="submit" id="trimite" tabindex="5" value="Submit" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>
<script type="text/javascript">/*<![CDATA[*/(function(a){a.fn.autoResize=function(j){var b=a.extend({onResize:function(){},animate:true,animateDuration:150,animateCallback:function(){},extraSpace:20,limit:1000},j);this.filter('textarea').each(function(){var c=a(this).css({resize:'none','overflow-y':'hidden'}),k=c.height(),f=(function(){var l=['height','width','lineHeight','textDecoration','letterSpacing'],h={};a.each(l,function(d,e){h[e]=c.css(e)});return c.clone().removeAttr('id').removeAttr('name').css({position:'absolute',top:0,left:-9999}).css(h).insertBefore(c)})(),i=null,g=function(){f.height(0).val(a(this).val()).scrollTop(10000);var d=Math.max(f.scrollTop(),k)+b.extraSpace,e=a(this).add(f);if(i===d){return}i=d;if(d>=b.limit){a(this).css('overflow-y','');return}b.onResize.call(this);b.animate&&c.css('display')==='block'?e.stop().animate({height:d},b.animateDuration,b.animateCallback):e.height(d)};c.unbind('.dynSiz').bind('keyup.dynSiz',g).bind('keydown.dynSiz',g).bind('change.dynSiz',g)});return this}})(jQuery);jQuery('textarea#comment').autoResize();/*]]>*/</script>
</div>
</div>
<?php endif; // If registration required and not logged in ?>

<?php else : // Comments are closed ?>
<a name="respond"></a><h2>Comments</h2><p><?php _e('Comments are closed.'); ?></p>
<?php endif; ?>