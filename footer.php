<div class="clr"></div>
</div>
<div id="footer" class="<?php if($nosidebar) echo "ext_footer"; ?>">

<span class="copy">&copy; <?=date('Y'); ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> - Powered by <a href="http://wordpress.org">Wordpress</a> - Theme by <a title="Andrei Luca" href="http://students.info.uaic.ro/~andrei.luca/">Andrei Luca</a></span>
</div>

</div>
</body>
</html>