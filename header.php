<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link //rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
<link rel="shortcut icon" type="image/ico" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_enqueue_script('jquery'); ?>
<?php wp_head(); ?>
<script src="<?php bloginfo('template_directory'); ?>/js/textresize.js" type="text/javascript"></script>
</head>
<body>
<div id="body">
<div id="header">
<h1><a id="head" title="<?php bloginfo("description"); ?>" href="<?php bloginfo("wpurl"); ?>"><?php bloginfo("description"); ?></a></h1>
<div title="+" class="textsize" id="large"></div>
<div title="-" class="textsize" id="small"></div>
</div>



<div id="menu<?php if($nosidebar) echo "_nosb"; ?>">
<div class="meniu">
<!-- <ul>
<li><a <?php //if(is_home()): ?>id="selectat"<?php //endif; ?> href="<?php //bloginfo('url'); ?>">Blog</a></li>
<?php //echo list_pages(); ?>

</ul> -->
<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_class' => 'main-menu-class' ) ); ?>
</div>
</div>

<div id="content" class="<?php if($nosidebar) echo "ext_content"; ?>">