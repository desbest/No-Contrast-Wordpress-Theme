<?php
// TWITTER SETTINGS
$enabled = false; // set to false to disable
$twitter_username = "wordpress";
?>
<?php get_header(); ?>
<div class="continut"> 
<?php if(is_home() && !is_paged() && $enabled == true) : ?>
<div class="blogger">
<div id="twitter_div"><ul id="twitter_update_list" style="list-style:none;margin:0;"></ul>
<a id="twitter-link" style="float:right;font-weight:700;" href="http://twitter.com/<?php echo $twitter_username; ?>">Follow me on Twitter</a>
</div>
<script src="http://twitter.com/javascripts/blogger.js" type="text/javascript"></script>
<script src="http://twitter.com/statuses/user_timeline/<?php echo $twitter_username; ?>.json?callback=twitterCallback2&count=1" type="text/javascript"></script>
<br style="clear:both;" /></div>
<div class="blogger_close"></div>
<?php endif; ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="post" id="post-<?php the_ID(); ?>">
<?php if(is_single()) : ?>
<h1><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
<?php else : ?>
<h2><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<?php endif; ?>
<?php $comentarii = get_comments_number(); if (($comentarii)>='100') $var_large=" extralarge"; else if($comentarii>='10' && $comentarii < '100') $var_large=" large"; else $var_large=""; ?>
<div class="data_blog<?php echo $var_large; ?>"><a title="<?php comments_number(); ?>" href="<?php the_permalink(); ?>#respond"><?php comments_number('0', '1', '%' ); ?></a></div>
<div class="meta"><a title="<?php the_author() ?>" href="<?php the_author_url(); ?> "><?php the_author(); ?></a> &#8212; <?php the_category(','); ?> @ <?php the_time('F j,  Y'); ?></div>

<div class="entry">
<?php the_content('<span>Read the entire post..</span>');  ?>
</div>


<?php if(is_single()) { the_tags(__('<div class="feedback">Tags: '), ', ', '</div>'); } ?>


</div>

<?php comments_template(); ?>

<?php endwhile; else: ?>
<h2>Eroare</h2>
<p><?php _e('Sorry, no posts matched your criteria.'); ?><br/><br/></p>
<h2><?php _e('Check out my last posts..'); ?></h2>
<ul class="error_li_listing">
<?php wp_get_archives('type=postbypost&limit=20'); ?>
</ul>
<?php endif; ?>
<?php posts_nav_link(' ', __('<span class="nav_links">&laquo; Recent articles</span>'), __('<span class="nav_links">Old articles &raquo;</span>')); ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
