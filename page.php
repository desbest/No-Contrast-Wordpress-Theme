<?php
get_header();
?>
<div class="continut"> 
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



<div class="post" id="post-<?php the_ID(); ?>" style="margin-bottom:1em;">
<h2><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<?php $comentarii = get_comments_number();  if (($comentarii)>='100') $var_large=" extralarge"; else if($comentarii>='10' && $comentarii < '100') $var_large=" large"; else $var_large=""; ?>
<div class="data_blog<?php echo $var_large; ?>"><a title="<?php comments_number(); ?>" href="<?php the_permalink(); ?>#respond"><?php comments_number('0', '1', '%' ); ?></a></div>

<div class="entry">
<?php the_content('<span>Read the entire post..</span>'); ?>
<?php wp_link_pages(); ?>
</div>


</div>

<?php comments_template(); // Get wp-comments.php template ?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php posts_nav_link(' &#8212; ', __('&laquo; Recent articles'), __('Old articles &raquo;')); ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
