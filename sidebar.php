<div class="sidebar">
<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
<input type="text" class="search" value="<?php the_search_query(); ?>" name="s" id="s" onfocus="this.value==this.defaultValue?this.value='':null" maxlength="24"/>
<input type="hidden" id="searchsubmit" value="Search" />
</form>

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar") ) : ?>
<div class="list"><?php _e('Recent articles'); ?></div>
<ul>
<?php
$rand_posts = get_posts('numberposts=10&cat=-37,-6,-53');
foreach( $rand_posts as $post ) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach; ?>
</ul><br/>



<div class="list"><?php _e('Categories'); ?></div>
<ul>
<?php wp_list_categories('title_li=0&exclude=37'); ?>
</ul>
<br/>


<div class="list"><?php _e('Meta'); ?></div>
<ul>
<?php wp_register(); ?>
<li><?php wp_loginout(); ?></li>
<?php wp_meta(); ?>
</ul>
<?php endif; ?>
</div>
