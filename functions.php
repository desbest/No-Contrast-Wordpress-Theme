<?php
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'functions.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');

if ( function_exists('register_sidebar') )
register_sidebar(array('id' => "sidebar", 'name' =>'Sidebar','before_widget' => '','after_widget' => '','before_title' => '<div class="list">','after_title' => '</div>'));

function register_my_menu() {
register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );


/**
 * Smart cache-busting
 * http://toscho.de/2008/frisches-layout/#comment-13
 */
/*
if ( !function_exists('fb_css_cache_buster') ) {
        function fb_css_cache_buster($info, $show) {
                if ($show == 'stylesheet_url') {

                        // Is there already a querystring? If so, add to the end of that.
                        if (strpos($pieces[1], '?') === false) {
                                return $info . "?" . filemtime(WP_CONTENT_DIR . $pieces[1]);
                        } else {
                                $morsels = explode("?", $pieces[1]);
                                return $info . "&" . filemtime(WP_CONTENT_DIR . $morsles[1]);
                        }
                } else {
                        return $info;
                }
        }

        add_filter('bloginfo_url', 'fb_css_cache_buster', 9999, 2);
}
*/



// SPAM PROTECT

function check_referrer() {
if (!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER'] == ��) {
wp_die( __('Please enable referrers in your browser, or, if you\'re a spammer, bugger off!') );
}
}

function list_pages(){
$top_list = wp_list_pages("echo=0&depth=0&title_li=");
$top_list = str_replace(array('">','</a>','<span><a','current_page_item"><a'),array('"><span>','</span></a>','<a','"><a id="selectat"'), $top_list);
return $top_list;
}

// LOCALIZATION

load_theme_textdomain('lightword', get_template_directory() . '/lang');

remove_action('wp_head', 'wp_generator');
remove_filter('the_content', 'wptexturize');
//wp_deregister_script('prototype');
//wp_deregister_script('jquery');
?>